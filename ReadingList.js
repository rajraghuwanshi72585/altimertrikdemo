({ 
    doInit : function(component, event, helper) { 
        helper.getData(component, event); 
    },
    handleRowAction: function (cmp, event, helper) {
        var rowaction = event.getParam('action');
        var row = event.getParam('row');
        var str = rowaction.name;  
        var isDelete = true;
        var strmsg = 'The Book has been Removed successfully.';
        var strerrormsg = "Failed to Remove The Book."
        if(str == 'Done_Reading'){
            isDelete = false;
            strmsg = 'The Book has been Updated successfully.';
            strerrormsg = "Failed to Update The Book."
        } 
        var action = cmp.get("c.UpdateBook");  
        action.setParams({    
            BookId : row.Id ,
            isDelete  :  isDelete 
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();  
            if (state === "SUCCESS") { 
                helper.getData(cmp, event);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message":  strmsg, 
                    "type": 'success' 
                });
                toastEvent.fire(); 
            } 
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": strerrormsg,
                    "type": 'Error'
                });
                toastEvent.fire();
            }
            
        });
        $A.enqueueAction(action); 
    }
})