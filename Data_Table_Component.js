({     
    doInit : function(component, event, helper) { 
        component.set('v.columns', [ 
            {label: 'Preview', fieldName: 'Preview', type: 'url' , iconName: 'utility:preview'},
            {label: 'Title', fieldName: 'Title', type: 'text', sortable: true,iconName: 'standard:pricebook' },  
            {label: 'ISBN', fieldName: 'ISBN', type: 'text', sortable: true, iconName: 'utility:info_alt' },
            {label: 'Author', fieldName: 'Author', type: 'text', sortable: true, iconName: 'utility:user'  },
            {label: 'Description', fieldName: 'Description', type: 'text',iconName: 'utility:description' },
            {label: 'Ratings', fieldName: 'Ratings', type: 'decimal', sortable: false , iconName: 'utility:rating', cellAttributes: { alignment: 'center'}  }, 
            {label: '',iconName: 'utility:add', type: 'action', typeAttributes: { rowActions: component.get('v.lstReadingList') }, cellAttributes: { alignment: 'center'}  }
        ]);  
    },
    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);  
        setTimeout(function() {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection'); 
            cmp.set("v.sortedBy", fieldName);
            cmp.set("v.sortedDirection", sortDirection);
            helper.sortData(cmp, fieldName, sortDirection);
            cmp.set('v.isLoading', false);
        }, 0);
    },
    handleRowAction: function (cmp, event, helper) {
        var rowaction = event.getParam('action');
        var row = event.getParam('row');
        var str = rowaction.name;  
        var objReadlistId = rowaction.name; 
        if(rowaction.name === 'New_Reading'){
            cmp.set('v.rowdata', row);
            cmp.set('v.isModelopen', true); 
        }     
        else {  
            var action = cmp.get("c.SaveBook"); 
            action.setParams({    
                strObjbook : JSON.stringify(row),
                strRLId  : str,
                isOld : true 
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();  
                if (state === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "The Book has been add successfully.", 
                        "type": 'success' 
                    });
                    toastEvent.fire();
                } 
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Failed to add to book.",
                        "type": 'Error'
                    });
                    toastEvent.fire();
                }  
            });
            $A.enqueueAction(action);
        }  
    }
})