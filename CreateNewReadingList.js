({
    closeModel : function(component, event, helper) {
        component.set('v.strReadingListName', '');
        component.set('v.isModelopen', false);
    },
    save : function(component, event, helper) { 
        if(component.get('v.strReadingListName') == '')
            component.find('input').showHelpMessageIfInvalid(); 
        else{  
            component.set('v.isLoading', true);
            var row = component.get("v.rowdata");             
            var action = component.get("c.SaveBook"); 
            var str = component.get("v.strReadingListName");
            action.setParams({    
                strObjbook : JSON.stringify(row),
                strRLId  : str,
                isOld : false 
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();  
                if (state === "SUCCESS" && response.getReturnValue() == "Success") { 
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "The ReadingList has been created successfully Book has been add to it.", 
                        "type": 'success' 
                    });
                    toastEvent.fire();
                    
                    console.log('reloadlist list',component.get('v.reloadlist'));
                    if(component.get('v.reloadlist'))
                        component.set('v.reloadlist', false); 
                    else
                        component.set('v.reloadlist', true);
                     
                    component.set('v.isModelopen', false);
                    component.set('v.strReadingListName', ''); 
                } 
                else{ 
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Failed to save ReadingList.",
                        "type": 'Error'
                    });
                    toastEvent.fire(); 
                    component.set('v.isModelopen', false);
                    component.set('v.strReadingListName', '');  
                }  
            });
            $A.enqueueAction(action);
        }
        
    }
})