({ 
    getData: function (component, event) {
        var action = component.get("c.getReadingList"); 
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            if (state === "SUCCESS") {  
                var response = response.getReturnValue(); 
                console.log('response',response);
                component.set('v.lstReadingList',response);
                component.set('v.columns', [ 
                    {label: 'PreviewLink', fieldName: 'PreviewLink__c', type: 'url'},
                    {label: 'Author', fieldName: 'Author__c', type: 'text'},
                    {label: 'ISBN', fieldName: 'ISBN__c', type: 'text' },
					{label: 'Done Reading?', initialWidth: 50,fieldName: 'Read__c', type: 'boolean',  cellAttributes: { alignment: 'center'} },
                    {label: 'Done/Remove', type: 'action', typeAttributes: { rowActions: [{name:'Done_Reading', label : 'Mark as Read?'},{name:'Remove', label : 'Remove'}] }, cellAttributes: { alignment: 'center'}  }
                    
                ]);
            } 
        });
        $A.enqueueAction(action);
    }
})