({
    helperMethod : function(cmp, evt) { 
        var queryTerm = cmp.find('enter-search').get('v.value'); 
        var keyval = cmp.get('v.selectedValue') +':'+ queryTerm;
        var action = cmp.get("c.getInitialDataSet"); 
        action.setParams({
            keyValue : keyval
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            if (state === "SUCCESS") {
                var response = response.getReturnValue(); 
                cmp.set('v.lstBooks',response.lstGoogleBooks);
                cmp.set('v.lstReadingList',response.lstReadingList);
            }
            cmp.set('v.issearching', false);
        });
        $A.enqueueAction(action); 
    }
})