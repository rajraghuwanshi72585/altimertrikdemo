({
    doInit : function(component, event, helper) {  
        var url = $A.get('$Resource.LibraryImage');
        component.set('v.backgroundImageURL',url); 
        component.set('v.options', [ 
            {  label: 'Author' ,value:"Author" , selected : true },
            {  label: 'Title' ,value:"Title"  },
            {  label: 'ISBN' ,value:"ISBN"  }
            
        ]); 
    },
    
    handleChange: function (cmp, evt,helper) {  
        helper.helperMethod(cmp, evt);
    },
    handleKeyUp: function (cmp, evt,helper ) { 
        var isEnterKey = evt.keyCode === 13; 
        if (isEnterKey) {
            cmp.set('v.issearching', true);  
            helper.helperMethod(cmp, evt); 
        }
    }
})